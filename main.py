from tasky import *

tasky = Tasky()

def clean_dir(data):
	print('This task will clear the directory thanks to', data['task_name'])

def upload_data():
	print('This task will upload data to custom server')


tasky.task('clean_dir', clean_dir)
tasky.task('upload_data', upload_data)

tasky.run()

tasky.lists()