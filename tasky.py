class Tasky:

	def __init__(self):

		self.tasks = []
	
	def task(self, name, fn):

		obj = {
			'task_name': name
		}

		self.tasks.append({
			'name': name,
			'fn': fn,
			'data': obj
		})
	
	def run(self):

		for task in self.tasks:

			try:
				task['fn'](task['data'])
			except:
				task['fn']()
	
	def lists(self):
		print(list(map(lambda t: t['name'], self.tasks)))


