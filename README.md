# Tasky

Simple task runner. There is no dependency. Everything is in the methods you write

# Usage

Import tasky to your project and initialize it

```python
tasky = Tasky()
```

## Write your methods

```python
def clean_dir(data):
	print('This task will clear the directory thanks to', data['task_name'])

def upload_data():
	print('This task will upload data to custom server')
```


## Create your tasks

```python
tasky.task('clean_dir', clean_dir)
tasky.task('upload_data', upload_data)
```

## Run Your Tasks

```python
tasky.run()
```

## Show Your Tasks

```python
tasky.lists()
```